# MariaDB Dokumentation

<table>
  <tr>
    <th style="vertical-align:top">Autoren</th>
    <th style="vertical-align:top">Klasse</th>
    <th style="vertical-align:top">Version</th>
    <th style="vertical-align:top">Datum</th>
  </tr>

  <tr>
    <td style="vertical-align:top">Carlo Troxler<br>Silas Fries<br>Nevio Kiener</td>
    <td style="vertical-align:top">INF22cL</td>
    <td style="vertical-align:top">1.6</td>
    <td style="vertical-align:top">03.06.2024</td>
  </tr>
</table>

<br><br><br>


# Inhalt

1. [Projektmanagement](#projektmanagement-ctrsfrnki)
2. [MariaDB](#mariadb-ctr)
3. [Daten mit Frigg generieren](#daten-mit-frigg-generieren-nki)
4. [Datenbank Installation](#datenbank-installation-nki)
5. [Datenbank erstellen](#datenbank-erstellen-sfr)
6. [Benutzer erstellen](#benutzer-erstellen-sfr)
7. [Tabellen erstellen](#tabellen-erstellen-sfr)
8. [View erstellen](#view-erstellen-sfr)
9. [Daten mit .csv Datei importieren](#daten-mit-csv-datei-importieren-sfr)
10. [Daten in Tabellen einfügen](#daten-in-tabellen-einfügen-ctr)
11. [gna Webservice](#gna-webservice-ctr)
12. [gna Webservice automatisch starten](#gna-webservice-automatisch-starten-ctr)
13. [pfSense Firewall einrichten](#pfsense-firewall-einrichten-ctr)
14. [gna Testing](#gna-testing-ctr)

<br><br><br>


# Projektmanagement [ctr/sfr/nki]

**Projektmitglieder**
Name|Kürzel|Funktion
---|---|---
Carlo Troxler|ctr|System Engineer
Silas Fries|sfr|System Engineer
Nevio Kiener|nki|System Engineer
<br><br>

**SOLL und IST Planung | ctr**

Meilenstein|IST|SOLL|Status
---|---|---|---
MS1: Datenbank installieren|26.02.2024|26.02.2024|<span style="color:green">erledigt
MS2: Datensatz erstellen|29.02.2024|04.03.2024|<span style="color:green">erledigt
MS3: Datenbank/-struktur erstellen|10.03.2024|11.03.2024|<span style="color:green">erledigt
MS4: User erstellen|26.02.2024|18.03.2024|<span style="color:green">erledigt
MS5: Daten importieren|12.03.2024|25.03.2024|<span style="color:green">erledigt
MS6: View implementieren|14.03.2024|15.04.2024|<span style="color:green">erledigt
MS7: Applikation einbinden|16.03.2024|22.04.2024|<span style="color:green">erledigt
Abnahme LB1|13.05.2024|13./20./27.5.|<span style="color:green">erledigt
Projektmeeting mit LP|18.03.2024|18.03.2024|<span style="color:green">erledigt
<br><br>

**SOLL und IST Planung | sfr**

Meilenstein|IST|SOLL|Status
---|---|---|---
MS1: Datenbank installieren|04.03.2024|26.02.2024|<span style="color:green">erledigt
MS2: Datensatz erstellen|04.03.2024|04.03.2024|<span style="color:green">erledigt
MS3: Datenbank/-struktur erstellen|11.03.2024|11.03.2024|<span style="color:green">erledigt
MS4: User erstellen|11.03.2024|18.03.2024|<span style="color:green">erledigt
MS5: Daten importieren|15.05.2024|25.03.2024|<span style="color:green">erledigt
MS6: View implementieren|22.04.2024|15.04.2024|<span style="color:green">erledigt
MS7: Applikation einbinden|06.05.2024|06.05.2024|<span style="color:green">erledigt
Abnahme LB1|13.05.2024|13.05.2024|<span style="color:green">erledigt
Projektmeeting mit LP|18.03.2024|18.03.2024|<span style="color:green">erledigt
<br><br>

**SOLL und IST Planung | nki**


Meilenstein|IST|SOLL|Status
---|---|---|---
MS1: Datenbank installieren|04.03.2024|18.03.2024|<span style="color:green">erledigt
MS2: Datensatz erstellen|01.04.2024|15.04.2024|<span style="color:green">erledigt
MS3: Datenbank/-struktur erstellen|15.04.2024|29.04.2024|<span style="color:green">erledigt
MS4: User erstellen|29.04.2024|06.05.2024|<span style="color:green">erledigt
MS5: Daten importieren|13.05.2024|06.05.2024|<span style="color:yellow">in Arbeit
MS6: View implementieren|06.05.2024|06.05.2024|<span style="color:red">offen
MS7: Applikation einbinden|06.05.2024|06.05.2024|<span style="color:red">offen
Abnahme LB1|13.05.2024|13.05.2024|<span style="color:green">erledigt
Projektmeeting mit LP|06.05.2024|13.05.2024|<span style="color:green">erledigt
<br><br><br>


# MariaDB [ctr]

- Gegründet von Monty Windenius -> Datenbank Name von Tochter inspiriert
- Kostenlose Open Source -> GPLv2
- Kostende Enterprise Version -> zusätzliche Features, Support
- Fork von MySQL im Jahr 2009
- Grossunternehmen verwenden MariaDB
  - Google 
  - Wikipedia
  - WordPress.com

<br>

Vorteile|Nachteile
---|---
Open Source|Support nicht so stark wie bei MS SQL
Sehr stabil|Kostenlose Version bietet nicht alle Enterprise Funktionen
Performant|
<br>

"MariaDB ist ein freies, relationales Open-Source-Datenbankmanagementsystem, das durch eine Abspaltung (Fork) aus MySQL entstanden ist. Das Projekt wurde von MySQLs früherem Hauptentwickler Michael Widenius initiiert, der auch die Storage-Engine Aria entwickelte, auf welcher MariaDB ursprünglich aufbaute (die Software-Schicht, welche die Basisfunktionalität der Datenbank enthält, d. h. das Erstellen, Lesen, Ändern, Löschen von Daten).
Da Oracle die Markenrechte an MySQL hält, mussten neue Namen für das Datenbanksystem und dessen Storage-Engines gefunden werden. Der Name MariaDB geht auf Widenius’ jüngere Tochter Maria zurück, wie schon MySQL auf seine ältere Tochter My."[^1]

[^1]: https://de.wikipedia.org/wiki/MariaDB
<br><br><br>
<br>


# Daten mit Frigg generieren [nki]

**Datensatz-Generator frigg auf Datenbank-Server installieren**
```
sudo apt install gdebi
sudo gdebi frigg_04.all.deb
```

```
Vorname:
Nachname:
Klasse:
```
<br>

**Datensatz generieren**
```
sudo frigg .
```

Falls Fehlermeldung "Command already defined: main at /usr/local/share/perl/5.30.0/OptArgs2.pm line 154." erscheint:\
Datei in `/user/local/share/perl/<version/OptArgs2.pm` mit Datei aus `~/Schreibtisch/M141/Datensatzgenerator_frigg/OptArgs2.pm` ersetzen

```
sudo cp -rf ~/Schreibtisch/M141/Datensatzgenerator_frigg/OptArgs2.pm /usr/lo-cal/share/perl/5.34.0/OptArgs2.pm
```

Anschliessend kann Datensatz als .csv Datei generiert werden

```
sudo frigg /opt/frigg > hans_muster.csv
```
<br>

**Datensatz mit Excel bearbeiten**

Damit die maximale Grösse vom Datentyp `VARCHAR` bestimmt werden kann:

1. Spalte links von jeweiligem Datenfeld erstelle
2. =LÄNGE(@F:F) > berechnet die Zeichenanzahl pro Zelle in der jeweiligen Spalte

<br><br><br>

 
# Datenbank Installation [nki]

**MariaDB installieren, starten und bei Neustart aktivieren auf DB01 Server**
```
$ sudo apt update
$ sudo apt install mariadb-server
$ sudo systemctl start mariadb
$ sudo systemctl enable mariadb
```
<br>

**MySQL Secure Installation durchführen**
```
$ sudo mysql_secure_installation
```
```
Enter current password for root (enter for none): 
Switch to unix_socket authentication [Y/n] n
Change the root password? [Y/n] n
Remove anonymous users? [Y/n] y
Disallow root login remotely? [Y/n] y
Remove test database and access to it? [Y/n] y
Reload privilege tables now? [Y/n] y
```
<br>

**Anmeldung auf MariaDB Datenbank**
```
$ sudo mysql -u root -p
```


**General Log aktivieren**
```
$ sudo mysql -u root -p
```


<br><br><br>


# Datenbank erstellen [sfr]
```
MariaDB [(none)]> CREATE DATABASE m141_YYYYMMDD;

MariaDB [(none)]> USE m141_YYYYMMDD;
Database changed
MariaDB [(m141_YYYYMMDD)]>
```
 

<br><br><br>


# Benutzer erstellen [sfr]
**Benutzer bbzwdb, READ AND WRITE auf '*', localhost**
```
CREATE USER 'bbzwdb'@'localhost' IDENTIFIED BY 'Sursee6210';
GRANT ALL PRIVILEGES ON *.* TO 'bbzwdb'@'localhost' WITH GRANT OPTION;
```
<br>

**Benutzer bbzwgast, READ auf A_check, localhost**
```
CREATE USER 'bbzwgast'@'localhost' IDENTIFIED BY 'Sursee6210';
GRANT SELECT ON m141_YYYYMMDD.A_check TO 'bbzwgast'@'localhost';
```
<br>

**Benutzer services, READ auf '*', APP01 Server**
```
CREATE USER 'services'@'10.10.1.104' IDENTIFIED BY 'Sursee6210';
GRANT SELECT ON m141_YYYYMMDD.* TO 'services'@'10.10.1.104';
```


<br><br><br>

 

# Tabellen erstellen [sfr]
```
CREATE TABLE T_type (
 PK_type_ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 name VARCHAR(50)
);

CREATE TABLE T_tag (
 PK_tag_ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 name VARCHAR(250)
);

CREATE TABLE T_systemuser (
 PK_systemuser_ID INT NOT NULL PRIMARY KEY,
 name VARCHAR(50)
);

CREATE TABLE T_usergroup (
 PK_usergroup_ID INT NOT NULL PRIMARY KEY,
 name VARCHAR(50)
);

CREATE TABLE T_data (
 PK_data_ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 digest VARCHAR(50),
 content BLOB,
 size INT,
 compression INT,
 FK_type_ID INT,
 FOREIGN KEY (FK_type_ID) REFERENCES T_type(PK_type_ID)
);

CREATE TABLE ZT_data_tag (
 PK_data_tag_ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 FK_data_ID INT,
 FK_tag_ID INT,
 FOREIGN KEY (FK_data_ID) REFERENCES T_data(PK_data_ID),
 FOREIGN KEY (FK_tag_ID) REFERENCES T_tag(PK_tag_ID)
);

CREATE TABLE T_meta (
 PK_meta_ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 path VARCHAR(250),
 perm VARCHAR(6),
 time INT,
 FK_data_ID INT,
 FK_systemuser_ID INT,
 FK_usergroup_ID INT,
 FOREIGN KEY (FK_data_ID) REFERENCES T_data(PK_data_ID),
 FOREIGN KEY (FK_systemuser_ID) REFERENCES T_systemuser(PK_systemuser_ID),
 FOREIGN KEY (FK_usergroup_ID) REFERENCES T_usergroup(PK_usergroup_ID)
);

CREATE TABLE T_import (
 count VARCHAR (10),
 digest VARCHAR (50),
 path VARCHAR (250),
 size INT,
 type VARCHAR (50),
 perm VARCHAR (6),
 PK_systemuser_ID INT,
 user VARCHAR (50),
 PK_usergroup_ID INT,
 `group` VARCHAR (50),
 time INT,
 compression INT,
 content BLOB
);
```
```
SELECT * FROM T_type LIMIT 3;
SELECT * FROM T_systemuser LIMIT 3;
SELECT * FROM T_usergroup LIMIT 3;
SELECT * FROM T_tag LIMIT 3;
SELECT PK_data_ID, digest, LEFT(content, 10) AS content_preview, size, compression, FK_type_ID FROM T_data LIMIT 5;
SELECT * FROM T_meta LIMIT 3;
SELECT * FROM ZT_data_tag LIMIT 3;
```
<br><br><br>


# View erstellen [sfr]
```
CREATE VIEW A_check AS
SELECT 	
  T_data.`digest` as `digest`,
  T_meta.`path` as `path`,
  T_data.`size` as `size`,
  T_type.`name` as `type`,
  T_meta.`perm` as `mode`,
  T_systemuser.`PK_systemuser_ID` as `uid`,
  T_systemuser.`name` as `user`,
  T_usergroup.`PK_usergroup_ID` as `gid`,
  T_usergroup.`name` as `group`,
  T_meta.`time` as `time`,
  T_data.`compression` as `compression`,
  T_data.`content` as `data`
FROM T_meta
  JOIN T_data on FK_data_id = PK_data_ID
  JOIN T_type on FK_type_ID = PK_type_id
  JOIN T_usergroup on FK_usergroup_ID = PK_usergroup_id
  JOIN T_systemuser on FK_systemuser_ID = PK_systemuser_id;
```
<br><br><br>


# Daten mit .csv Datei importieren [sfr]
Daten Import von CSV File in Zwischentabelle T_import\
Spalten getrennt durch ',' / Zeilen getrennt durch Absatz / 1. Linie wird ignoriert\
Auswahl, welche Spalten importiert werden sollen\
Zuordnung von DB-Spalten zu Import-Spalten
<br>
```
LOAD DATA LOCAL INFILE '/home/bbzw/Schreibtisch/Datensatz/vorname_nachname.csv'
    INTO TABLE T_import
    FIELDS TERMINATED BY ','
    LINES TERMINATED BY '\n'
    IGNORE 1 LINES
    (@count, @digest, @path, @size, @type, @mode,
    @uid, @user, @gid, @group, @time, @compression, @data)
    SET count=@count, 
        digest=@digest,
        path=@path,
        size=@size,
        type=@type,
        perm=@mode,
        PK_systemuser_ID=@uid,
        user=@user,
        PK_usergroup_ID=@gid,
        `group`=@group,
        time=@time,
        compression=@compression,
        content=@data;
```

<br><br><br>

 

# Daten in Tabellen einfügen [ctr]
Zuerst Tabellen ohne Beziehungen befüllen, dann Tabellen mit Beziehungen\
Tabellen mit Beziehungen mit INNER JOIN verbinden
<br>
```
INSERT INTO T_type (name)
SELECT DISTINCT type FROM T_import;

INSERT INTO T_systemuser (PK_systemuser_ID, name)
SELECT DISTINCT PK_systemuser_ID, user FROM T_import;

INSERT INTO T_usergroup (PK_usergroup_ID, name)
SELECT DISTINCT PK_usergroup_ID, `group` FROM T_import;

INSERT INTO T_data (digest, size, compression, content, FK_type_ID)
SELECT DISTINCT digest, size, compression, content, T_type.PK_type_ID
FROM T_import
INNER JOIN T_type ON T_type.name = T_import.type;

INSERT INTO T_meta (path, perm, time, FK_data_ID, FK_systemuser_ID, FK_usergroup_ID)
SELECT DISTINCT path, perm, time, T_data.PK_data_ID, T_systemuser.PK_systemuser_ID, T_usergroup.PK_usergroup_ID 
FROM T_import
INNER JOIN T_data ON T_data.digest = T_import.digest
INNER JOIN T_systemuser ON T_systemuser.name = T_import.user
INNER JOIN T_usergroup ON T_usergroup.name = T_import.group;
```

<br><br><br>

 

# gna Webservice [ctr]
**gna installieren auf APP01 Server**
```
$ sudo gdebi gna_0.6_all.deb
```
<br>

**Webservice starten**
```
$ gna

Web application available at http://127.0.0.1:3000
```
<br>

 **gna Konfiguration bearbeiten**
```
$ sudo nano /etc/gna.conf.yml
```
<br>

**Verbindungsangaben für gna**
```
---
# Applikation
app:
  # Ausgeben von DB-Fehlern auf der Konsole
  print_error: 5
  # Ausgeben von DB-Fehlern im Web-GUI, Abbrechen des Programms
  raise_error: 1
  # Webserver Port
  port: 3000

##########################################
# Verbingungsangaben zum Datenbankserver #
##########################################
db:
  dsn: 'DBI:mysql:m141_YYYYMMDD;host=db01;port=3306'
  user: services
  pass: Sursee6210
```
<br>

**SQL-Queries für gna**
```
######################################################################
# SQL-Queries zur Anbindung der Applikation an die Datenbankstruktur #
# ****************************************************************** #
#                                                                    #
# Die hier per default konfigurierten Queries sind nur Platzhalter,  #
# damit «etwas» im GUI angezeigt wird! Passen Sie die Queries für    #
# Ihre Datenbank an!                                                 #
# Die Variabel-Benennung gibt Hinweise auf involvierte Tabellen. Das #
# «leere» GUI liefert ebenfalls Hinweise über die Art der Daten. In  #
# der Dokumentation sind zusätzlich Screenshots eines befüllten GUI. #
######################################################################

# Project Information
project:
  # Dieser Query muss nur angepasst werden, falls Ihre konkrete DB diesen aus Syntax-Gründen nicht ausführen kann.
  author: 'select content from T_data join T_meta on T_data.PK_data_ID = T_meta.FK_data_ID where T_meta.path = "/opt/frigg/admin/projekt.txt";'
# SQL-Queries für die Startseite
index:
  # Select String: Angaben zur Datenbank liefern. Muss allenfalls auf DB angepasst werden.
  sql_db_version: 'select version();'

# SQL-Queries für die Übersichts-Seite /overview
overview:
  # Select Nummer: Anzahl Einträge pro Tabelle
  sql_data_count: 'select count(*) from T_data;'
  sql_meta_count: 'select count(*) from T_meta;'
  sql_type_count: 'select count(*) from T_type;'
  sql_tag_count:  'select count(*) from T_tag;' 
  sql_usr_count:  'select count(*) from T_systemuser;'
  sql_grp_count:  'select count(*) from T_usergroup;'
  # Select Nummer: Aufsummiertes Attribute «size»
  sql_data_size_sum: 'select sum(size) from T_data;'
  sql_meta_size_sum: 'select sum(size) from T_meta join T_data on T_data.PK_data_ID = T_meta.FK_data_ID;'
  # Select Nummer: Aufsummierte in der Datenbank belegte Grösse des Attributes «content» in der Tabelle «Data»
  sql_data_contentsize_sum: 'select sum(length(content)) from T_data;'
  # Select Nummer: Aufsummierte in der Datenbank belegte Grösse des Attributes «content» in der Tabelle «Data»
  # im JOIN mit der Tabelle «Meta»
  sql_meta_contentsize_sum: 'SELECT SUM(LENGTH(content)) FROM T_meta JOIN T_data ON T_data.PK_data_ID = T_meta.FK_data_ID;'

# SQL-Query für die «Browse» Seite /browse
browse:
  # Select digest, path, size, type, perm, usr, grp, time, compression mit binded Parameter path und limit
  sql_browseview_bind: "SELECT T_data.digest AS digest,T_meta.path AS path,T_data.size AS size,T_type.name AS type,T_meta.perm AS perm,T_systemuser.name AS usr,T_usergroup.name AS grp,T_meta.time AS time,T_data.compression AS compression FROM T_data JOIN T_type ON T_data.FK_type_ID = T_type.PK_type_ID JOIN T_meta ON T_data.PK_data_ID = T_meta.FK_data_ID JOIN T_systemuser ON T_meta.FK_systemuser_ID = T_systemuser.PK_systemuser_ID JOIN T_usergroup ON T_meta.FK_usergroup_ID = T_usergroup.PK_usergroup_ID WHERE T_meta.path LIKE ? ORDER BY T_data.digest OFFSET 0 ROWS FETCH FIRST ? ROWS ONLY;"
# SQL-Queries für die Daten-Seite /data
data:
  # Select String: Attribut «content» mit binded Parameter «digest»
  sql_content_bind: "select content from T_data where digest = ?;"
  # Select String: Attribut «path» mit binded Parameter «digest»
  sql_contentpaths_bind: "select path from T_meta JOIN T_data ON T_data.PK_data_ID = T_meta.FK_data_ID WHERE T_data.digest = ?;"
  # Insert String: binded Parameter für Attribut «Tag.name»,
  # bereits vorhandene Namen sollen ignoriert werden (keine doppelte Ablage)
  sql_tag_insert_unique_bind: 'INSERT INTO T_tag (name) VALUES (?);'
  # Insert Nummern: FKs für «Data» und «Tag» in Tabelle «DataTag» einfügen,
  # mit binden Parameter «Data.id» und «Tag.name»
  sql_datatag_insert_unique_bind: 'INSERT INTO ZT_data_tag (FK_data_ID, FK_tag_ID) VALUES ((SELECT PK_data_ID FROM T_data WHERE digest = ?),(SELECT PK_tag_ID FROM T_tag WHERE name = ?));'
  # Select String: Tag anzeigen, mit binden Parameter «Data.id/digest»
  sql_tag_for_digest_bind: "SELECT T_tag.name FROM ZT_data_tag JOIN T_tag ON ZT_data_tag.FK_tag_ID = T_tag.PK_tag_ID JOIN T_data ON ZT_data_tag.FK_data_ID = T_data.PK_data_ID WHERE T_data.digest = ?;"

# SQL-Query für Anzeige Dateiinhalt auf der Seite /raw
raw:
  # Select String: Dateityp anhand von binded Parameter «digest» auswählen
  # (nicht im GUI sichtbar, für HTTP-Content-Type)
  sql_contenttype_bind: "SELECT T_type.name FROM T_data JOIN T_type ON T_data.FK_type_ID = T_type.PK_type_ID WHERE T_data.digest = ? LIMIT 1;"
tags:
  # Select String: Anzeige aller gesetzten Tags
  sql_tags_all_ordered: "SELECT name FROM T_tag;"
  # Select String: Anzeige aller Dateipfade für ein Tag
  sql_metapaths_for_tag_bind: "SELECT path FROM T_meta JOIN ZT_data_tag ON T_meta.FK_data_ID = ZT_data_tag.FK_data_ID JOIN T_tag ON ZT_data_tag.FK_tag_ID = T_tag.PK_tag_ID WHERE T_tag.name = ?;"
```

<br><br><br>

 
# gna Webservice automatisch starten [ctr]
Erstellen Sie eine Service-Datei:
<br>
```
sudo nano /etc/systemd/system/gna.service
```

Definieren Sie den Service:
<br>

```
[Unit]
Description=GNA Webservice
After=network.target

[Service]
Type=simple
ExecStart=/usr/bin/gna
Restart=always

[Install]
WantedBy=multi-user.target
```

Laden Sie die geänderten Systemd-Konfigurationen:
<br>
```
sudo systemctl daemon-reload
```

Starten Sie den Service:
<br>
```
sudo systemctl start gna
```

Aktivieren Sie den Service:
<br>
```
sudo systemctl enable gna
```


<br><br><br>



# pfSense Firewall einrichten [ctr]
**Netzwerkdiagramm**\
<br>
![Alt text](https://gitlab.com/m141mariadb/mariadb-documentation/-/raw/main/_images/Netzwerk.png)

<br>

**Eigenschaften**\
pfSense 2.6.0-RELEASE\
FreeBSD/amd64
<br><br>

**WebGUI öffnen**\
https://192.168.1.1:4333
<br><br>

**Port-Freigabe über Rules einrichten**\
Es muss eine Regel erstellt werden, damit die Applikation gna auf die DB (falls im Intranet) zugreifen kann.\
Der Port ist im File: gna.conf.yaml ersichtlich: 3306

Firewall > Regeln > DMZ

Protokoll: IPv4 TCP/UDP\
Quelle: DMZ net\
Port: *\
Ziel: INTRANET net\
Port: 3306\
Gateway: *\
Queue: nicht gesetzt\
Beschreibung: DMZ Richtung IntranetNET
<br><br>

**Zugriff von WAN auf Webservice gna**\
Damit auf den Webservice gna von extern zugegriffen werden kann, muss eine Port-Weiterleitung auf der Firewall eingerichtet werden (Destination NAT).

Firewall > NAT > Port Weiterleitung

Schnittstelle: WAN\
Protokoll: TCP\
Quelladresse: *\
Quellports: *\
Zieladresse: WAN address\
Zielports: 3000 (HBCI)\
NAT IP: 10.10.1.100\
NAT-Ports: 3000 (HBCI)\
Beschreibung: WAN Richtung DMZ Webservice gna
<br><br>

<span style="color:red;font-size:20px;">**WICHTIG!**</span>\
Damit die VMs untereinander kommunizieren können, müssen auf allen VMs die Firewall deaktiviert werden.
```
$ sudo ufw disable
```
<br><br><br>


# gna Testing [ctr]

<br>
Die Datenbank Webseite kann über http://localhost:3000 erreicht werden.
Auf der Startseite werden die Autoren, Datenbank Verbindung und die gna Version über einen SELECT Befehl ausgelesen.
<br>
<br>


![Alt text](https://gitlab.com/m141mariadb/mariadb-documentation/-/raw/ea2673d22b55e5ff9e9df9d056d6acfd0b606f4c/_images/2024-05-27_09_11_59-M141_Ubuntu_APP01_-_VMware_Workstation.png)

<br>
Auf der Übersichts-Seite werden die Anzahl Datensätze von den einzelnen Tabellen angezeigt, sowie die zusammengerechneten Datei-Grössen von der Data und Meta Tabelle.
Hier kann überprüft werden, ob die Anzahl Datensätze korrekt von der CSV-Datei in die Datenbank importiert wurden.
<br>
<br>

![Alt text](https://gitlab.com/m141mariadb/mariadb-documentation/-/raw/ea2673d22b55e5ff9e9df9d056d6acfd0b606f4c/_images/2024-05-27_09_12_40-M141_Ubuntu_APP01_-_VMware_Workstation.png)

<br>
Auf der Browser-Seite werden die einzelnen Tabellen durch einen SELECT Befehl angezeigt. Hier kann über die URL-Leiste die Anzahl der angezeigten Datensätze gesteuert und über einen Textfilter nach Dateien gesucht werden.
<br>
<br>

![Alt text](https://gitlab.com/m141mariadb/mariadb-documentation/-/raw/ea2673d22b55e5ff9e9df9d056d6acfd0b606f4c/_images/2024-05-27_09_13_06-M141_Ubuntu_APP01_-_VMware_Workstation.png)

<br>
Auf der Data-Seite wird eine einzelne Datei angezeigt mit dem dazugehörigen Digest, Path, Tags und dem Content in Base64 verschlüsselt.
<br>
<br>

![Alt text](https://gitlab.com/m141mariadb/mariadb-documentation/-/raw/ea2673d22b55e5ff9e9df9d056d6acfd0b606f4c/_images/2024-05-27_09_15_32-M141_Ubuntu_APP01_-_VMware_Workstation.png)

<br>
Entpackt man die Datei über den UNPACK-Link, erscheint auf der RAW-Seite der Datei, welche mit Base64 wieder entschlüsselt wurde.
<br>
<br>

![Alt text](https://gitlab.com/m141mariadb/mariadb-documentation/-/raw/ea2673d22b55e5ff9e9df9d056d6acfd0b606f4c/_images/2024-05-27_09_16_02-M141_Ubuntu_APP01_-_VMware_Workstation.png)

<br>
Auf der Tags-Seite werden die Tags angezeigt, welche auf der Data-Seite gesetzt wurden. Wählt man einen Tag durch den Link, wird zusätzlich der Pfad zu der Datei angezeigt.
<br>
<br>

![Alt text](https://gitlab.com/m141mariadb/mariadb-documentation/-/raw/ea2673d22b55e5ff9e9df9d056d6acfd0b606f4c/_images/2024-05-27_09_16_24-M141_Ubuntu_APP01_-_VMware_Workstation.png)
 

<br><br><br>